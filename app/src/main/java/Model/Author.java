package Model;

/**
 * Created by su on 13/10/17.
 */

public class Author {
    private int id;
    private String nama;
    private String nim;
    private String api_key;
    private String telpon;
    private String email;
    private String alamat;

    public Author(int id,String nama,String nim,String api_key,
                  String telpon,String email,String alamat){
        this.id=id;
        this.nama=nama;
        this.api_key=api_key;
        this.telpon=telpon;
        this.email=email;
        this.alamat=alamat;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getNim() {
        return nim;
    }

    public void setNim(String nim) {
        this.nim = nim;
    }

    public String getApi_key() {
        return api_key;
    }

    public void setApi_key(String api_key) {
        this.api_key = api_key;
    }

    public String getTelpon() {
        return telpon;
    }

    public void setTelpon(String telpon) {
        this.telpon = telpon;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }
}
